#include "EDBaseAfx.h"
#include "TrayIconWnd.h"

BEGIN_MSG_MAP(CTrayIconWnd)
	MESSAGE_HANDLER_EX(m_CbMsg, OnTrayIcon)
	MESSAGE_HANDLER_EX(m_RestartMsg, OnTaskbarRestart)
END_MSG_MAP()
CTrayIconWnd::CTrayIconWnd()
{
	m_CbMsg		     = ::RegisterWindowMessage(_T("TaskbarNotifyMsg"));// 接收回调消息
	m_RestartMsg     = ::RegisterWindowMessage(_T("TaskbarCreated"));  // 当任务栏崩溃时会接收到此消息

	m_hNotifyWnd     = NULL;
	m_bInstall	     = false;
	m_bExitBalloon   = true;
	m_hBalloonThread = NULL;
	m_dwOldTime      = 0;

	memset(&m_Nid, 0, sizeof(NOTIFYICONDATA));
	m_Nid.cbSize     = sizeof(NOTIFYICONDATA);
}

CTrayIconWnd::~CTrayIconWnd()
{
	UnInstallTrayIcon();
}

bool CTrayIconWnd::InstallTrayIcon(LPCWSTR lpszToolTip, HWND hNotifyWnd, HICON hIcon, UINT nID)
{
	do 
	{
		if (NULL == hNotifyWnd || !::IsWindow(hNotifyWnd))
		{
			break;
		}
		m_hNotifyWnd = hNotifyWnd;
		if (!IsWindow())
		{
			ATOM Atom = g_pDMApp->GetClassAtom();
			DMCWnd::CreateWindowEx((LPCWSTR)Atom,NULL,WS_OVERLAPPEDWINDOW,0,0,0,1,1,NULL,0);
			if (NULL == m_hWnd)
			{
				break;
			}
		}

		m_Nid.hWnd	= m_hWnd;
		m_Nid.uID   = nID;
		m_Nid.hIcon = hIcon;
		m_Nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
		m_Nid.uCallbackMessage = m_CbMsg;

		if (NULL != lpszToolTip)
		{
			wcscpy_s(m_Nid.szTip,countof(m_Nid.szTip),lpszToolTip);
		}

		m_bInstall = ::Shell_NotifyIconW(NIM_ADD, &m_Nid)? true : false;
		ShowWindow(SW_HIDE);

		if (hIcon)
		{
			::DestroyIcon(hIcon);
		}
	} while (false);
	return m_bInstall;
}

bool CTrayIconWnd::UnInstallTrayIcon()
{
	if (false == m_bInstall)
	{
		return false;
	}
	ExitBalloonThread();
	if (IsWindow())
	{
		DestroyWindow();
	}
	m_Nid.uFlags = 0;
	m_bExitBalloon = true;
	m_bInstall = false;
	return ::Shell_NotifyIconW(NIM_DELETE, &m_Nid)? true : false;
}

bool CTrayIconWnd::SetTipText(LPCWSTR lpszTipText)
{
	if (NULL == lpszTipText)
	{
		return false;
	}

	m_Nid.uFlags = NIF_TIP;
	wcscpy_s(m_Nid.szTip,countof(m_Nid.szTip),lpszTipText);
	return ::Shell_NotifyIconW(NIM_MODIFY, &m_Nid)? true : false;
}

void CTrayIconWnd::SetNewNotifyHWnd(HWND hNotifyWnd)
{
	m_hNotifyWnd = hNotifyWnd;
}

bool CTrayIconWnd::ModifyTrayIcon(HICON hIcon)
{
	if (NULL == hIcon)
	{
		return false;
	}
	m_Nid.hIcon=hIcon; 
	BOOL bRet = ::Shell_NotifyIcon(NIM_MODIFY,&m_Nid);
	if (hIcon)
	{
		::DestroyIcon(hIcon);
	}
	return !!bRet;
}

bool CTrayIconWnd::ModifyTrayIcon(UINT nID, HINSTANCE hInstance)
{
	if (NULL == hInstance)
	{
		hInstance = GetModuleHandleW(NULL);
	}
	HICON hIcon = ::LoadIcon(hInstance, MAKEINTRESOURCE(nID));
	if (NULL == hIcon)
	{
		return false;
	}

	m_Nid.hIcon=hIcon;
	BOOL bRet = ::Shell_NotifyIcon(NIM_MODIFY,&m_Nid);
	if (hIcon)
	{
		::DestroyIcon(hIcon);
	}
	return !!bRet;
}


bool CTrayIconWnd::SetBalloonDetails(LPCWSTR lpszBalloonText, LPCWSTR lpszBalloonCaption, UINT nTimeout,
										DWORD style, HICON hUserIcon, bool bNoSound)
{
	bool bRet = false;
	do 
	{
		HideBalloon();
		ExitBalloonThread();
		m_Nid.uFlags	 |= NIF_INFO;
		m_Nid.dwInfoFlags = style;
		m_Nid.uTimeout	  = nTimeout;

		if (bNoSound)
		{
			m_Nid.dwInfoFlags |= NIIF_NOSOUND;
		}
		else
		{
			m_Nid.dwInfoFlags &= ~NIIF_NOSOUND;
		}

		memset(m_Nid.szInfoTitle,0,countof(m_Nid.szInfoTitle));
		memset(m_Nid.szInfo,0,countof(m_Nid.szInfo));
		if (lpszBalloonCaption)
		{
			wcscpy_s(m_Nid.szInfoTitle,countof(m_Nid.szInfoTitle),lpszBalloonCaption);
		}
		if (lpszBalloonText)
		{
			wcscpy_s(m_Nid.szInfo,countof(m_Nid.szInfo),lpszBalloonText);
		}

		bRet = ::Shell_NotifyIconW(NIM_MODIFY, &m_Nid)? true : false;
		if (bRet)
		{
			m_dwOldTime = GetTickCount();
			m_bExitBalloon = false;
			m_hBalloonThread = ::CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)StartTimeCheck,this,0,NULL);
		}

	} while (false);
	return bRet;
}

bool CTrayIconWnd::HideBalloon()
{
	bool bRet = false;
	do 
	{
		m_bExitBalloon = true;
		if (NULL == m_Nid.hWnd || !::IsWindow(m_Nid.hWnd))
		{
			break;
		}
		m_Nid.uFlags = NIF_INFO;
		memset(m_Nid.szInfoTitle,0,countof(m_Nid.szInfoTitle));
		memset(m_Nid.szInfo,0,countof(m_Nid.szInfo));

		bRet = ::Shell_NotifyIconW(NIM_MODIFY, &m_Nid)? true : false;
	} while (false);
	return bRet;
}

LRESULT CTrayIconWnd::OnTrayIcon(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	UINT Msg = (UINT)LOWORD(lParam);
	switch (Msg)
	{
	case WM_RBUTTONUP:
		{
			if (NULL != m_hNotifyWnd && ::IsWindow(m_hNotifyWnd))
			{
				::PostMessage(m_hNotifyWnd, WM_TRAY_RBUTTONUP, 0 ,NULL);
			}
		}
		break;

	case WM_LBUTTONDBLCLK:
	case WM_LBUTTONUP:
		{
			if (NULL != m_hNotifyWnd && ::IsWindow(m_hNotifyWnd))
			{
				// 这里可以发个消息让窗口置顶
				::PostMessage(m_hNotifyWnd, WM_TRAY_LBUTTONUP, 0 ,NULL);	
			}
		}
		break;
	case 0x404://win10下汽泡退出会触发此消息
		{
			m_bExitBalloon = true;
		}
		break;
	default:
		break;
	}
	return 0;
}

LRESULT CTrayIconWnd::OnTaskbarRestart(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	if (m_bInstall && m_Nid.hWnd && ::IsWindow(m_Nid.hWnd))
	{
		m_Nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
		return ::Shell_NotifyIcon(NIM_ADD, &m_Nid);
	}
	return -3;
}

void CTrayIconWnd::ExitBalloonThread()
{
	m_bExitBalloon = true;
	if (m_hBalloonThread)
	{// 多次调用SetBalloonDetails时，强制以前的辅助线程已退出
		DWORD dwIndex = -1;
		if (S_OK != ::CoWaitForMultipleHandles(0,1000,1,&m_hBalloonThread,&dwIndex))
		{
			TerminateThread(m_hBalloonThread,IDOK);
			m_hBalloonThread = NULL;
		}
		DM_CLOSEHANDLE(m_hBalloonThread);
	}
}


// static 
DWORD CTrayIconWnd::StartTimeCheck(LPVOID lp)
{
	CTrayIconWnd* pThis = (CTrayIconWnd*)lp;
	DWORD dwOldTime = pThis->m_dwOldTime;
	DWORD dwTimeOut = pThis->m_Nid.uTimeout;
	while (!pThis->m_bExitBalloon)
	{
		DWORD dwCurTime = GetTickCount();
		if (dwCurTime - dwOldTime >= dwTimeOut)
		{
			pThis->HideBalloon();
			return TRUE;
		}
		Sleep(10);
	}
	return TRUE;
}


