#include "EDBaseAfx.h"
#include "DumpReport.h"
#include <Dbghelp.h>
#pragma comment(lib,"Dbghelp.lib")

bool g_bDumpReport = false;			///< 是否已成功挂机崩溃上报模块

void DumpReport::InitDumpReport(LPCWSTR lpVersion /*= NULL*/)
{
	do  
	{ 
		SetInvalidHandle();
		AddExceptionHandle();
#ifndef _DEBUG
		wchar_t szDllDir[MAX_PATH] = {0};
		DM::GetRootFullPath(L".\\crashreport.dll",szDllDir,MAX_PATH);
		HMODULE hdll = ::LoadLibraryW(szDllDir);
		if (NULL == hdll)
		{
			break;
		} 

		typedef void (__cdecl *fun_AddCustomReportFile)(LPCWSTR lpszPath);
		typedef int  (__cdecl *fun_InitBugReport)(LPCWSTR lpszProductId, LPCWSTR lpszProductVer, LPCWSTR lpszProductName);
		typedef void (__cdecl *fun_SetShowReportUI)(BOOL bShow);

		fun_InitBugReport InitBugReport = (fun_InitBugReport)::GetProcAddress(hdll,"InitBugReport");
		fun_AddCustomReportFile AddCustomReportFile = (fun_AddCustomReportFile)::GetProcAddress(hdll,"AddCustomReportFile");
		fun_SetShowReportUI SetShowReportUI = (fun_SetShowReportUI)::GetProcAddress(hdll,"SetShowReportUI");
		if (NULL == InitBugReport || NULL == AddCustomReportFile ||NULL == SetShowReportUI)
		{
			break;
		}

		if (TRUE == InitBugReport(L"4366game", lpVersion, L"4366游戏大厅"))
		{
			wchar_t szLogDir[MAX_PATH] = {0};
			DM::GetRootFullPath(L".\\gplatform.log",szLogDir,MAX_PATH);
			AddCustomReportFile(szLogDir);
			SetShowReportUI(FALSE);
			g_bDumpReport = true;
		}  
#endif
	} while (false);
}  

LPTOP_LEVEL_EXCEPTION_FILTER WINAPI DumpReport::TempSetUnhandledExceptionFilter( LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter )
{
	return NULL;
}

BOOL DumpReport::DumpToFile(LPCTSTR szPath, EXCEPTION_POINTERS *pException)
{
	HANDLE hDumpFile = ::CreateFile(szPath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);  
	if (INVALID_HANDLE_VALUE == hDumpFile)
	{
		return FALSE;
	}

	MINIDUMP_EXCEPTION_INFORMATION dumpInfo;  
	dumpInfo.ExceptionPointers = pException;  
	dumpInfo.ThreadId = ::GetCurrentThreadId();  
	dumpInfo.ClientPointers = TRUE;  
	BOOL bRet = ::MiniDumpWriteDump(::GetCurrentProcess(), ::GetCurrentProcessId(), hDumpFile, MiniDumpNormal, &dumpInfo, NULL, NULL);  
	::CloseHandle(hDumpFile);  
	return bRet;
}

LONG WINAPI DumpReport::UnhandledExceptionFilterEx( _EXCEPTION_POINTERS *pException )
{
	if (g_bDumpReport) // 已挂接崩溃上报模块，无需保存DUMP文件
	{
		return EXCEPTION_EXECUTE_HANDLER;
	}

	wchar_t szPath[MAX_PATH] = { 0 };
	::GetModuleFileNameW(NULL, szPath, MAX_PATH);
	lstrcatW(szPath, L".dmp");
	LOG_USER("DUMP到文件：%s\n",szPath);
	if (DumpToFile(szPath, pException))
	{
		return EXCEPTION_EXECUTE_HANDLER;
	}
	LOG_USER("DUMP到文件失败！\n");
	return EXCEPTION_CONTINUE_SEARCH;
}

LONG WINAPI DumpReport::ExitUnhandledExceptionFilterEx(_EXCEPTION_POINTERS *pException)
{
	// 退出时不再做崩溃上报
#if _DEBUG
	wchar_t szPath[MAX_PATH] = { 0 };
	::GetModuleFileNameW(NULL, szPath, MAX_PATH);
	lstrcatW(szPath, L"exitprocess.dmp");
	LOG_USER("退出进程时发生DUMP,DUMP文件：%s\n",szPath);
	DumpToFile(szPath, pException);
#endif
	return EXCEPTION_EXECUTE_HANDLER;
}

void DumpReport::MyPureCallHandler()
{
	throw std::invalid_argument("");
}

void DumpReport::MyInvalidParameterHandler( const wchar_t* expression, const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved )
{
	throw std::invalid_argument("");
}

void DumpReport::SetInvalidHandle()
{
	m_preIph = _set_invalid_parameter_handler(MyInvalidParameterHandler);
	m_prePch = _set_purecall_handler(MyPureCallHandler);   //At application, this call can stop show the error message box.
}

void DumpReport::UnSetInvalidHandle()
{
	if (m_preIph)_set_invalid_parameter_handler(m_preIph);
	if (m_prePch)_set_purecall_handler(m_prePch); //At application this can stop show the error message box.
}

BOOL DumpReport::AddExceptionHandle()
{
	m_preFilter = ::SetUnhandledExceptionFilter(UnhandledExceptionFilterEx);
	//PreventSetUnhandledExceptionFilter();
	return TRUE;
}


BOOL DumpReport::AddExitExceptionHandle()
{
	::SetUnhandledExceptionFilter(ExitUnhandledExceptionFilterEx);
	return TRUE;
}

BOOL DumpReport::RemoveExceptionHandle()
{
	if (NULL != m_preFilter)
	{
		::SetUnhandledExceptionFilter(m_preFilter);
		m_preFilter = NULL;
	}
	return TRUE;
}

BOOL DumpReport::PreventSetUnhandledExceptionFilter()
{
	HMODULE hKernel32 = /*LoadLibrary*/::GetModuleHandle(_T("kernel32.dll"));
	if (hKernel32 == NULL)
		return FALSE;

	void *pOrgEntry = ::GetProcAddress(hKernel32, "SetUnhandledExceptionFilter");
	if(pOrgEntry == NULL)
		return FALSE;

	unsigned char newJump[5];
	DWORD dwOrgEntryAddr = (DWORD)pOrgEntry;
	dwOrgEntryAddr += 5; //jump instruction has 5 byte space.

	void *pNewFunc = &TempSetUnhandledExceptionFilter;
	DWORD dwNewEntryAddr = (DWORD)pNewFunc;
	DWORD dwRelativeAddr = dwNewEntryAddr - dwOrgEntryAddr;

	newJump[0] = 0xE9;  //jump
	memcpy(&newJump[1], &dwRelativeAddr, sizeof(DWORD));
	SIZE_T bytesWritten;
	DWORD dwOldFlag, dwTempFlag;
	::VirtualProtect(pOrgEntry, 5, PAGE_READWRITE, &dwOldFlag);
	BOOL bRet = ::WriteProcessMemory(::GetCurrentProcess(), pOrgEntry, newJump, 5, &bytesWritten);
	::VirtualProtect(pOrgEntry, 5, dwOldFlag, &dwTempFlag);
	return bRet;
}
