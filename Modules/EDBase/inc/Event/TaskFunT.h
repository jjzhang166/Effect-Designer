// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	TaskFunT.h
// File mark:   
// File summary:支持构造最多7个输入参数(已封装)，5个输出参数(未封装)的函数
//              1.使用NewRunnableFunction构造普通函数，NewRunnableMethod构造成员函数:NewRunnableFunction(&FUN, arg0, arg1, arg2)，NewRunnableMethod(&CLS::FUN,CLS::THIS,arg0, arg1, arg2)
//              2.注意构造NewRunnableMethod时，使用的是ICancelableTask, 默认类支持DMRefNum,类在运行前AddRef，运行后ReleaseRef,可以通过声明DISABLE_RUNNABLE_METHOD_REFCOUNT(类名)取消
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-11
// ----------------------------------------------------------------
#pragma once
/// <summary>
///		任务的基类
/// </summary>
class ITask : public DMRefNum
{
public:
	virtual ~ITask(){}
	virtual void Run() = 0;
};
typedef ITask*			ITaskPtr;
class ICancelableTask : public ITask
{
public:
	virtual void Cancel() = 0;	///< 不是所有的任务都支持取消操作.
};

///---------------------------------------------------------------------------------------------------------------------------
template <typename T>
struct TupleTraits 
{
	typedef T ValueType;
	typedef T& RefType;
	typedef const T& ParamType;
};

template <typename T>
struct TupleTraits<T&> 
{
	typedef T ValueType;
	typedef T& RefType;
	typedef T& ParamType;
};

struct Tuple0 
{
	typedef Tuple0 ValueTuple;
	typedef Tuple0 RefTuple;
};

template <typename A>
struct Tuple1 
{
public:
	typedef A TypeA;
	typedef Tuple1<typename TupleTraits<A>::ValueType> ValueTuple;
	typedef Tuple1<typename TupleTraits<A>::RefType> RefTuple;

	Tuple1() {}
	explicit Tuple1(typename TupleTraits<A>::ParamType a) 
		: a(a) 
	{
	}

	A a;
};

template <typename A, typename B>
struct Tuple2 
{
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef Tuple2<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType> ValueTuple;
	typedef Tuple2<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType> RefTuple;

	Tuple2() {}
	Tuple2(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b)
		: a(a), b(b) 
	{
	}

	A a;
	B b;
};

template <typename A, typename B, typename C>
struct Tuple3 
{
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef C TypeC;
	typedef Tuple3<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType,
		typename TupleTraits<C>::ValueType> ValueTuple;
	typedef Tuple3<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType,
		typename TupleTraits<C>::RefType> RefTuple;

	Tuple3() {}
	Tuple3(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b,
		typename TupleTraits<C>::ParamType c)
		: a(a), b(b), c(c)
	{
	}

	A a;
	B b;
	C c;
};

template <typename A, typename B, typename C, typename D>
struct Tuple4 {
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef C TypeC;
	typedef D TypeD;
	typedef Tuple4<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType,
		typename TupleTraits<C>::ValueType,
		typename TupleTraits<D>::ValueType> ValueTuple;
	typedef Tuple4<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType,
		typename TupleTraits<C>::RefType,
		typename TupleTraits<D>::RefType> RefTuple;

	Tuple4() {}
	Tuple4(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b,
		typename TupleTraits<C>::ParamType c,
		typename TupleTraits<D>::ParamType d)
		: a(a), b(b), c(c), d(d) 
	{
	}

	A a;
	B b;
	C c;
	D d;
};

template <typename A, typename B, typename C, typename D, typename E>
struct Tuple5 {
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef C TypeC;
	typedef D TypeD;
	typedef E TypeE;
	typedef Tuple5<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType,
		typename TupleTraits<C>::ValueType,
		typename TupleTraits<D>::ValueType,
		typename TupleTraits<E>::ValueType> ValueTuple;
	typedef Tuple5<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType,
		typename TupleTraits<C>::RefType,
		typename TupleTraits<D>::RefType,
		typename TupleTraits<E>::RefType> RefTuple;

	Tuple5() {}
	Tuple5(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b,
		typename TupleTraits<C>::ParamType c,
		typename TupleTraits<D>::ParamType d,
		typename TupleTraits<E>::ParamType e)
		: a(a), b(b), c(c), d(d), e(e) 
	{
	}

	A a;
	B b;
	C c;
	D d;
	E e;
};

template <typename A, typename B, typename C, typename D, typename E, typename F>
struct Tuple6 {
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef C TypeC;
	typedef D TypeD;
	typedef E TypeE;
	typedef F TypeF;
	typedef Tuple6<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType,
		typename TupleTraits<C>::ValueType,
		typename TupleTraits<D>::ValueType,
		typename TupleTraits<E>::ValueType,
		typename TupleTraits<F>::ValueType> ValueTuple;
	typedef Tuple6<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType,
		typename TupleTraits<C>::RefType,
		typename TupleTraits<D>::RefType,
		typename TupleTraits<E>::RefType,
		typename TupleTraits<F>::RefType> RefTuple;

	Tuple6() {}
	Tuple6(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b,
		typename TupleTraits<C>::ParamType c,
		typename TupleTraits<D>::ParamType d,
		typename TupleTraits<E>::ParamType e,
		typename TupleTraits<F>::ParamType f)
		: a(a), b(b), c(c), d(d), e(e), f(f) 
	{
	}

	A a;
	B b;
	C c;
	D d;
	E e;
	F f;
};

template <typename A, typename B, typename C, typename D, typename E, typename F, typename G>
struct Tuple7 {
public:
	typedef A TypeA;
	typedef B TypeB;
	typedef C TypeC;
	typedef D TypeD;
	typedef E TypeE;
	typedef F TypeF;
	typedef G TypeG;
	typedef Tuple7<typename TupleTraits<A>::ValueType,
		typename TupleTraits<B>::ValueType,
		typename TupleTraits<C>::ValueType,
		typename TupleTraits<D>::ValueType,
		typename TupleTraits<E>::ValueType,
		typename TupleTraits<F>::ValueType,
		typename TupleTraits<G>::ValueType> ValueTuple;
	typedef Tuple7<typename TupleTraits<A>::RefType,
		typename TupleTraits<B>::RefType,
		typename TupleTraits<C>::RefType,
		typename TupleTraits<D>::RefType,
		typename TupleTraits<E>::RefType,
		typename TupleTraits<F>::RefType,
		typename TupleTraits<G>::RefType> RefTuple;

	Tuple7() {}
	Tuple7(typename TupleTraits<A>::ParamType a,
		typename TupleTraits<B>::ParamType b,
		typename TupleTraits<C>::ParamType c,
		typename TupleTraits<D>::ParamType d,
		typename TupleTraits<E>::ParamType e,
		typename TupleTraits<F>::ParamType f,
		typename TupleTraits<G>::ParamType g)
		: a(a), b(b), c(c), d(d), e(e), f(f), g(g) 
	{
	}

	A a;
	B b;
	C c;
	D d;
	E e;
	F f;
	G g;
};

///-----------------------------------------------------------------------------------------------
// 构造Tuple
inline Tuple0 MakeTuple() 
{
	return Tuple0();
}

template <typename A>
inline Tuple1<A> MakeTuple(const A& a) 
{
	return Tuple1<A>(a);
}

template <typename A, typename B>
inline Tuple2<A, B> MakeTuple(const A& a, const B& b) 
{
	return Tuple2<A, B>(a, b);
}

template <typename A, typename B, typename C>
inline Tuple3<A, B, C> MakeTuple(const A& a, const B& b, const C& c) 
{
	return Tuple3<A, B, C>(a, b, c);
}

template <typename A, typename B, typename C, typename D>
inline Tuple4<A, B, C, D> MakeTuple(const A& a, const B& b, const C& c,
									const D& d) 
{
	return Tuple4<A, B, C, D>(a, b, c, d);
}

template <typename A, typename B, typename C, typename D, typename E>
inline Tuple5<A, B, C, D, E> MakeTuple(const A& a, const B& b, const C& c,
									   const D& d, const E& e) 
{
	return Tuple5<A, B, C, D, E>(a, b, c, d, e);
}

template <typename A, typename B, typename C, typename D, typename E, typename F>
inline Tuple6<A, B, C, D, E, F> MakeTuple(const A& a, const B& b, const C& c,
										  const D& d, const E& e, const F& f) 
{
	return Tuple6<A, B, C, D, E, F>(a, b, c, d, e, f);
}

template <typename A, typename B, typename C, typename D, typename E, typename F, typename G>
inline Tuple7<A, B, C, D, E, F, G> MakeTuple(const A& a, const B& b, const C& c,
											 const D& d, const E& e, const F& f,
											 const G& g) 
{
	return Tuple7<A, B, C, D, E, F, G>(a, b, c, d, e, f, g);
}

///-----------------------------------------------------------------------------------------------
// 构造引用Tuple
template <typename A>
inline Tuple1<A&> MakeRefTuple(A& a) 
{
	return Tuple1<A&>(a);
}

template <typename A, typename B>
inline Tuple2<A&, B&> MakeRefTuple(A& a, B& b) 
{
	return Tuple2<A&, B&>(a, b);
}

template <typename A, typename B, typename C>
inline Tuple3<A&, B&, C&> MakeRefTuple(A& a, B& b, C& c) 
{
	return Tuple3<A&, B&, C&>(a, b, c);
}

template <typename A, typename B, typename C, typename D>
inline Tuple4<A&, B&, C&, D&> MakeRefTuple(A& a, B& b, C& c, D& d) 
{
	return Tuple4<A&, B&, C&, D&>(a, b, c, d);
}

template <typename A, typename B, typename C, typename D, typename E>
inline Tuple5<A&, B&, C&, D&, E&> MakeRefTuple(A& a, B& b, C& c, D& d, E& e) 
{
	return Tuple5<A&, B&, C&, D&, E&>(a, b, c, d, e);
}

template <typename A, typename B, typename C, typename D, typename E, typename F>
inline Tuple6<A&, B&, C&, D&, E&, F&> MakeRefTuple(A& a, B& b, C& c, D& d, E& e,
												   F& f) 
{
	return Tuple6<A&, B&, C&, D&, E&, F&>(a, b, c, d, e, f);
}

template <typename A, typename B, typename C, typename D, typename E, typename F, typename G>
inline Tuple7<A&, B&, C&, D&, E&, F&, G&> MakeRefTuple(A& a, B& b, C& c, D& d,
													   E& e, F& f, G& g) 
{
	return Tuple7<A&, B&, C&, D&, E&, F&, G&>(a, b, c, d, e, f, g);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// 成员方法派发
template <class ObjT, class Method>
inline void DispatchToMethod(ObjT* obj, Method method, const Tuple0& arg) {
	(obj->*method)();
}

template <class ObjT, class Method, class A>
inline void DispatchToMethod(ObjT* obj, Method method, const A& arg) {
	(obj->*method)(arg);
}

template <class ObjT, class Method, class A>
inline void DispatchToMethod(ObjT* obj, Method method, const Tuple1<A>& arg) {
	(obj->*method)(arg.a);
}

template<class ObjT, class Method, class A, class B>
inline void DispatchToMethod(ObjT* obj,
							 Method method,
							 const Tuple2<A, B>& arg) {
								 (obj->*method)(arg.a, arg.b);
}

template<class ObjT, class Method, class A, class B, class C>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple3<A, B, C>& arg) {
								 (obj->*method)(arg.a, arg.b, arg.c);
}

template<class ObjT, class Method, class A, class B, class C, class D>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple4<A, B, C, D>& arg) {
								 (obj->*method)(arg.a, arg.b, arg.c, arg.d);
}

template<class ObjT, class Method, class A, class B, class C, class D, class E>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple5<A, B, C, D, E>& arg) {
								 (obj->*method)(arg.a, arg.b, arg.c, arg.d, arg.e);
}

template<class ObjT, class Method, class A, class B, class C, class D, class E,
class F>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<A, B, C, D, E, F>& arg) {
		(obj->*method)(arg.a, arg.b, arg.c, arg.d, arg.e, arg.f);
}

template<class ObjT, class Method, class A, class B, class C, class D, class E,
class F, class G>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple7<A, B, C, D, E, F, G>& arg) {
		(obj->*method)(arg.a, arg.b, arg.c, arg.d, arg.e, arg.f, arg.g);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// 普通函数
template <class Function>
inline void DispatchToFunction(Function function, const Tuple0& arg) {
	(*function)();
}

template <class Function, class A>
inline void DispatchToFunction(Function function, const A& arg) {
	(*function)(arg);
}

template <class Function, class A>
inline void DispatchToFunction(Function function, const Tuple1<A>& arg) {
	(*function)(arg.a);
}

template<class Function, class A, class B>
inline void DispatchToFunction(Function function, const Tuple2<A, B>& arg) {
	(*function)(arg.a, arg.b);
}

template<class Function, class A, class B, class C>
inline void DispatchToFunction(Function function, const Tuple3<A, B, C>& arg) {
	(*function)(arg.a, arg.b, arg.c);
}

template<class Function, class A, class B, class C, class D>
inline void DispatchToFunction(Function function,
							   const Tuple4<A, B, C, D>& arg) {
								   (*function)(arg.a, arg.b, arg.c, arg.d);
}

template<class Function, class A, class B, class C, class D, class E>
inline void DispatchToFunction(Function function,
							   const Tuple5<A, B, C, D, E>& arg) {
								   (*function)(arg.a, arg.b, arg.c, arg.d, arg.e);
}

template<class Function, class A, class B, class C, class D, class E, class F>
inline void DispatchToFunction(Function function,
							   const Tuple6<A, B, C, D, E, F>& arg) {
								   (*function)(arg.a, arg.b, arg.c, arg.d, arg.e, arg.f);
}


///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 0 out param (as a Tuple0).
template <class ObjT, class Method>
inline void DispatchToMethod(ObjT* obj,
							 Method method,
							 const Tuple0& arg, Tuple0*) {
								 (obj->*method)();
}

template <class ObjT, class Method, class A>
inline void DispatchToMethod(ObjT* obj, Method method, const A& arg, Tuple0*) {
	(obj->*method)(arg);
}

template <class ObjT, class Method, class A>
inline void DispatchToMethod(ObjT* obj,
							 Method method,
							 const Tuple1<A>& arg, Tuple0*) {
								 (obj->*method)(arg.a);
}

template<class ObjT, class Method, class A, class B>
inline void DispatchToMethod(ObjT* obj,
							 Method method,
							 const Tuple2<A, B>& arg, Tuple0*) {
								 (obj->*method)(arg.a, arg.b);
}

template<class ObjT, class Method, class A, class B, class C>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple3<A, B, C>& arg, Tuple0*) {
								 (obj->*method)(arg.a, arg.b, arg.c);
}

template<class ObjT, class Method, class A, class B, class C, class D>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple4<A, B, C, D>& arg, Tuple0*) {
								 (obj->*method)(arg.a, arg.b, arg.c, arg.d);
}

template<class ObjT, class Method, class A, class B, class C, class D, class E>
inline void DispatchToMethod(ObjT* obj, Method method,
							 const Tuple5<A, B, C, D, E>& arg, Tuple0*) {
								 (obj->*method)(arg.a, arg.b, arg.c, arg.d, arg.e);
}

template<class ObjT, class Method, class A, class B, class C, class D, class E,
class F>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<A, B, C, D, E, F>& arg, Tuple0*) {
		(obj->*method)(arg.a, arg.b, arg.c, arg.d, arg.e, arg.f);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 1 out param.
template<class ObjT, class Method,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple0& in,
	Tuple1<OutA>* out) {
		(obj->*method)(&out->a);
}

template<class ObjT, class Method, class InA,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const InA& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in, &out->a);
}

template<class ObjT, class Method, class InA,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple1<InA>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, &out->a);
}

template<class ObjT, class Method, class InA, class InB,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple2<InA, InB>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, in.b, &out->a);
}

template<class ObjT, class Method, class InA, class InB, class InC,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple3<InA, InB, InC>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, in.b, in.c, &out->a);
}

template<class ObjT, class Method, class InA, class InB, class InC, class InD,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple4<InA, InB, InC, InD>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, &out->a);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple5<InA, InB, InC, InD, InE>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, &out->a);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE, class InF,
class OutA>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<InA, InB, InC, InD, InE, InF>& in,
	Tuple1<OutA>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, in.f, &out->a);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 2 out param.
template<class ObjT, class Method,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple0& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(&out->a, &out->b);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const InA& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in, &out->a, &out->b);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple1<InA>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, &out->a, &out->b);
}

template<class ObjT, class Method, class InA, class InB,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple2<InA, InB>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, in.b, &out->a, &out->b);
}

template<class ObjT, class Method, class InA, class InB, class InC,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple3<InA, InB, InC>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, in.b, in.c, &out->a, &out->b);
}

template<class ObjT, class Method, class InA, class InB, class InC, class InD,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple4<InA, InB, InC, InD>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, &out->a, &out->b);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple5<InA, InB, InC, InD, InE>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, &out->a, &out->b);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE, class InF,
class OutA, class OutB>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<InA, InB, InC, InD, InE, InF>& in,
	Tuple2<OutA, OutB>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, in.f, &out->a, &out->b);
}
///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 3 out param.
template<class ObjT, class Method,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple0& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(&out->a, &out->b, &out->c);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const InA& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple1<InA>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method, class InA, class InB,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple2<InA, InB>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, in.b, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method, class InA, class InB, class InC,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple3<InA, InB, InC>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, in.b, in.c, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method, class InA, class InB, class InC, class InD,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple4<InA, InB, InC, InD>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple5<InA, InB, InC, InD, InE>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, &out->a, &out->b, &out->c);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE, class InF,
class OutA, class OutB, class OutC>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<InA, InB, InC, InD, InE, InF>& in,
	Tuple3<OutA, OutB, OutC>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, in.f, &out->a, &out->b, &out->c);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 4 out param.
template<class ObjT, class Method,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple0& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(&out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const InA& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in, &out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple1<InA>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, &out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method, class InA, class InB,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple2<InA, InB>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, in.b, &out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method, class InA, class InB, class InC,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple3<InA, InB, InC>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, in.b, in.c, &out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method, class InA, class InB, class InC, class InD,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple4<InA, InB, InC, InD>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, &out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple5<InA, InB, InC, InD, InE>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e,
			&out->a, &out->b, &out->c, &out->d);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE, class InF,
class OutA, class OutB, class OutC, class OutD>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<InA, InB, InC, InD, InE, InF>& in,
	Tuple4<OutA, OutB, OutC, OutD>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, in.f,
			&out->a, &out->b, &out->c, &out->d);
}

///------------------------------------------------------------------------------------------------------------------------------------函数包装处
// Dispatchers with 5 out param.
template<class ObjT, class Method,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple0& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(&out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const InA& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in, &out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method, class InA,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple1<InA>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, &out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method, class InA, class InB,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple2<InA, InB>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, in.b, &out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method, class InA, class InB, class InC,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple3<InA, InB, InC>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, in.b, in.c, &out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method, class InA, class InB, class InC, class InD,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple4<InA, InB, InC, InD>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, &out->a, &out->b, &out->c, &out->d,
			&out->e);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple5<InA, InB, InC, InD, InE>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e,
			&out->a, &out->b, &out->c, &out->d, &out->e);
}

template<class ObjT, class Method,
class InA, class InB, class InC, class InD, class InE, class InF,
class OutA, class OutB, class OutC, class OutD, class OutE>
	inline void DispatchToMethod(ObjT* obj, Method method,
	const Tuple6<InA, InB, InC, InD, InE, InF>& in,
	Tuple5<OutA, OutB, OutC, OutD, OutE>* out) {
		(obj->*method)(in.a, in.b, in.c, in.d, in.e, in.f,
			&out->a, &out->b, &out->c, &out->d, &out->e);
}

///------------------------------------------------------------------------------------------------------------------------------------构造可运行函数类
// 构造可运行函数类
const size_t kDeadTask = 0xDEAD7A53;

// 删除对象的Task.
template<class T>
class DeleteTask : public ICancelableTask
{
public:
	explicit DeleteTask(const T* obj) : m_obj(obj) {}
	virtual void Run()
	{
		delete m_obj;
	}
	virtual void Cancel()
	{
		m_obj = NULL;
	}

private:
	const T* m_obj;
};

// 调用对象Release()方法的Task.
template<class T>
class ReleaseTask : public ICancelableTask
{
public:
	explicit ReleaseTask(const T* obj) : m_obj(obj) {}
	virtual void Run()
	{
		if(m_obj)
		{
			m_obj->Release();
		}
	}
	virtual void Cancel()
	{
		m_obj = NULL;
	}

private:
	const T* m_obj;
};

// Equivalents for use by base::Bind().
template<typename T>
void DeletePointer(T* obj)
{
	delete obj;
}

template<typename T>
void ReleasePointer(T* obj)
{
	obj->Release();
}

// RunnableMethodTraits --------------------------------------------------------
//
// This traits-class is used by RunnableMethod to manage the lifetime of the
// callee object.  By default, it is assumed that the callee supports AddRef
// and Release methods.  A particular class can specialize this template to
// define other lifetime management.  For example, if the callee is known to
// live longer than the RunnableMethod object, then a RunnableMethodTraits
// struct could be defined with empty RetainCallee and ReleaseCallee methods.
//
// The DISABLE_RUNNABLE_METHOD_REFCOUNT macro is provided as a convenient way
// for declaring a RunnableMethodTraits that disables refcounting.

template<class T>
struct RunnableMethodTraits
{
	RunnableMethodTraits()
	{
	}

	~RunnableMethodTraits()
	{
	}


	void RetainCallee(T* obj)
	{
#ifndef NDEBUG
		// Catch NewRunnableMethod being called in an object's constructor.  This
		// isn't safe since the method can be invoked before the constructor
		// completes, causing the object to be deleted.
		//  如果引用的类对象引用计数原始为0,+1-1可能调用默认的delete
		obj->AddRef();
		obj->Release();
#endif
		obj->AddRef();
	}

	void ReleaseCallee(T* obj)
	{
		obj->Release();
	}
private:
};

// Convenience macro for declaring a RunnableMethodTraits that disables
// refcounting of a class.  This is useful if you know that the callee
// will outlive the RunnableMethod object and thus do not need the ref counts.
//
// The invocation of DISABLE_RUNNABLE_METHOD_REFCOUNT should be done at the
// global namespace scope.  Example:
//
//   namespace foo {
//   class Bar {
//     ...
//   };
//   }  // namespace foo
//
//   DISABLE_RUNNABLE_METHOD_REFCOUNT(foo::Bar)
//
// This is different from DISALLOW_COPY_AND_ASSIGN which is declared inside the
// class.
#define DISABLE_RUNNABLE_METHOD_REFCOUNT(TypeName) \
	template<> \
struct RunnableMethodTraits<TypeName> { \
	void RetainCallee(TypeName* manager) {} \
	void ReleaseCallee(TypeName* manager) {} \
}

// RunnableMethod and RunnableFunction -----------------------------------------
//
// Runnable methods are a type of task that call a function on an object when
// they are run. We implement both an object and a set of NewRunnableMethod and
// NewRunnableFunction functions for convenience. These functions are
// overloaded and will infer the template types, simplifying calling code.
//
// The template definitions all use the following names:
// T                - the class type of the object you're supplying
//                    this is not needed for the Static version of the call
// Method/Function  - the signature of a pointer to the method or function you
//                    want to call
// Param            - the parameter(s) to the method, possibly packed as a Tuple
// A                - the first parameter (if any) to the method
// B                - the second parameter (if any) to the method
//
// Put these all together and you get an object that can call a method whose
// signature is:
//   R T::MyFunction([A[, B]])
//
// Usage:
// PostTask(FROM_HERE, NewRunnableMethod(object, &Object::method[, a[, b]])
// PostTask(FROM_HERE, NewRunnableFunction(&function[, a[, b]])

// RunnableMethod and NewRunnableMethod implementation -------------------------


template<class T, class Method, class Params>
class RunnableMethod : public ICancelableTask
{
public:
	RunnableMethod(Method meth, T* obj, const Params& params)
		: m_obj(obj), m_meth(meth), m_params(params)
	{
		m_traits.RetainCallee(m_obj);
	}

	~RunnableMethod()
	{
		ReleaseCallee();
		m_obj = reinterpret_cast<T*>(kDeadTask);
	}

	virtual void Run()
	{
		if(m_obj)
		{
			DispatchToMethod(m_obj, m_meth, m_params);
		}
	}

	virtual void Cancel()
	{
		ReleaseCallee();
	}

private:
	void ReleaseCallee()
	{
		T* obj = m_obj;
		m_obj = NULL;
		if(obj)
		{
			m_traits.ReleaseCallee(obj);
		}
	}

	T*						 m_obj;
	Method					 m_meth;
	Params					 m_params;
	RunnableMethodTraits<T>  m_traits;
};

template<class T, class Method>
inline ICancelableTask* NewRunnableMethod(Method method, T* object)
{
	return new RunnableMethod<T, Method, Tuple0>(method, object, MakeTuple());
}

template<class T, class Method, class A>
inline ICancelableTask* NewRunnableMethod(Method method, T* object, const A& a)
{
	return new RunnableMethod<T, Method, Tuple1<A> >(method, object,
		MakeTuple(a));
}

template<class T, class Method, class A, class B>
inline ICancelableTask* NewRunnableMethod(Method method, T* object, 
										  const A& a, const B& b)
{
	return new RunnableMethod<T, Method, Tuple2<A, B> >(method, object,
		MakeTuple(a, b));
}

template<class T, class Method, class A, class B, class C>
inline ICancelableTask* NewRunnableMethod(Method method, T* object,
										  const A& a, const B& b, const C& c)
{
	return new RunnableMethod<T, Method, Tuple3<A, B, C> >(method, object,
		MakeTuple(a, b, c));
}

template<class T, class Method, class A, class B, class C, class D>
inline ICancelableTask* NewRunnableMethod(Method method, T* object,
										  const A& a, const B& b,
										  const C& c, const D& d)
{
	return new RunnableMethod<T, Method, Tuple4<A, B, C, D> >(method, object,
		MakeTuple(a, b, c, d));
}

template<class T, class Method, class A, class B, class C, class D, class E>
inline ICancelableTask* NewRunnableMethod(Method method, T* object,
										  const A& a, const B& b,
										  const C& c, const D& d, const E& e)
{
	return new RunnableMethod<T,
		Method,
		Tuple5<A, B, C, D, E> >(method, object,
		MakeTuple(a, b, c, d, e));
}

template<class T, class Method, class A, class B, class C, class D, class E,
class F>
	inline ICancelableTask* NewRunnableMethod(Method method, T* object,
	const A& a, const B& b,
	const C& c, const D& d, const E& e,
	const F& f)
{
	return new RunnableMethod<T,
		Method,
		Tuple6<A, B, C, D, E, F> >(method, object,
		MakeTuple(a, b, c, d, e, f));
}

template<class T, class Method, class A, class B, class C, class D, class E,
class F, class G>
	inline ICancelableTask* NewRunnableMethod(Method method, T* object,
	const A& a, const B& b,
	const C& c, const D& d, const E& e,
	const F& f, const G& g)
{
	return new RunnableMethod<T,
		Method,
		Tuple7<A, B, C, D, E, F, G> >(method, object,
		MakeTuple(a, b, c, d, e, f, g));
}


// RunnableFunction and NewRunnableFunction implementation ---------------------
template<class Function, class Params>
class RunnableFunction : public ITask
{
public:
	RunnableFunction(Function function, const Params& params)
		: function_(function), params_(params)
	{
	}

	~RunnableFunction()
	{
	//	function_ = reinterpret_cast<Function>(kDeadTask);
	}

	virtual void Run()
	{
		if(function_)
		{
			DispatchToFunction(function_, params_);
		}
	}

private:
	Function function_;
	Params params_;
};

template<class Function>
inline ITask* NewRunnableFunction(Function function)
{
	return new RunnableFunction<Function, Tuple0>(function, MakeTuple());
}

template<class Function, class A>
inline ITask* NewRunnableFunction(Function function, const A& a)
{
	return new RunnableFunction<Function, Tuple1<A> >(function, MakeTuple(a));
}

template<class Function, class A, class B>
inline ITask* NewRunnableFunction(Function function, const A& a, const B& b)
{
	return new RunnableFunction<Function, Tuple2<A, B> >(function,
		MakeTuple(a, b));
}

template<class Function, class A, class B, class C>
inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
								  const C& c)
{
	return new RunnableFunction<Function, Tuple3<A, B, C> >(function,
		MakeTuple(a, b, c));
}

template<class Function, class A, class B, class C, class D>
inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
								  const C& c, const D& d)
{
	return new RunnableFunction<Function, Tuple4<A, B, C, D> >(function,
		MakeTuple(a, b, c, d));
}

template<class Function, class A, class B, class C, class D, class E>
inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
								  const C& c, const D& d, const E& e)
{
	return new RunnableFunction<Function, Tuple5<A, B, C, D, E> >(function,
		MakeTuple(a, b, c, d, e));
}

template<class Function, class A, class B, class C, class D, class E,
class F>
	inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
	const C& c, const D& d, const E& e,
	const F& f)
{
	return new RunnableFunction<Function, Tuple6<A, B, C, D, E, F> >(function,
		MakeTuple(a, b, c, d, e, f));
}

template<class Function, class A, class B, class C, class D, class E,
class F, class G>
	inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
	const C& c, const D& d, const E& e, const F& f,
	const G& g)
{
	return new RunnableFunction<Function, Tuple7<A, B, C, D, E, F, G> >(function,
		MakeTuple(a, b, c, d, e, f, g));
}

template<class Function, class A, class B, class C, class D, class E,
class F, class G, class H>
	inline ITask* NewRunnableFunction(Function function, const A& a, const B& b,
	const C& c, const D& d, const E& e, const F& f,
	const G& g, const H& h)
{
	return new RunnableFunction<Function, Tuple8<A, B, C, D, E, F, G, H> >(
		function, MakeTuple(a, b, c, d, e, f, g, h));
}