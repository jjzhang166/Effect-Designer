// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EventGlobal.h 
// File mark:   
// File summary:全局导出类
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-6
// ----------------------------------------------------------------
#pragma once
#include "Bundle.h"

class ED_EXPORT EventGlobal
{
public:
	static void initEvent(DWORD dwThreadId);
	static void uninitEvent(DWORD dwThreadId);
	static bool existEvent(LPCSTR pszEventName);
	static void removeEvent(LPCSTR pszEventName);
	static void removeEventIfThis(void* pThis);
	static void removeAllEvents(void);
	static bool connectEvent(LPCSTR pszEventName, const GPSlot& slot, int group = -1);
	static bool connectUiEvent(LPCSTR pszEventName, const GPSlot& slot, int group = -1);
	static bool connectAsyncEvent(LPCSTR pszEventName, const GPSlot& slot, int group = -1);
	static void fireEvent(LPCSTR pszEventName);
	static void fireEvent(LPCSTR pszEventName, Bundle& args);
};