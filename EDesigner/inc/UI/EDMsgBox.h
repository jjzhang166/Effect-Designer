// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDMsgBox.h 
// File mark:   
// File summary:专为弹框准备的
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-3-13
// ----------------------------------------------------------------
#pragma once

int ED_MessageBox(LPCWSTR lpText,UINT uType = MB_OK, LPCWSTR lpCaption = L"MSG",HWND hWnd = NULL);
class EDMsgBox : public DMHDialog
{
public:
	EDMsgBox(LPCWSTR lpszXmlId = L"ds_msgbox");
	~EDMsgBox();
	int MessageBox(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType); 

private:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_MAP()
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	void OnSize(UINT nType, CSize size);
	DMCode OnBtnClick(int uID);
private:
	CStringW                        m_strXmlId;
	CStringW                        m_strText;
	CStringW                        m_strCaption;
	UINT                            m_uType;
};