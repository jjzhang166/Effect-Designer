//-------------------------------------------------------
// Copyright (c) DuiMagic
// All rights reserved.
// 
// File Name: SDKPreviewWnd.h 
// File Des: 实时预览窗口
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-4-9	1.0		
//-------------------------------------------------------
#pragma once
#include <chrono>
#include <mutex>
#include <condition_variable>

namespace via
{
	class VenusInterface;
	class InterfaceInvoker;
}

class SDKPreviewWnd : public DMHWnd
{
public:
	SDKPreviewWnd();	
	~SDKPreviewWnd();
	DECLARE_MESSAGE_MAP()// 仿MFC消息映射宏

	//---------------------------------------------------
	// Function Des: 消息分发系列函数
	//---------------------------------------------------
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	void OnDUITimer(char id);

	void OnSize(UINT nType, CSize size);	
public:

	void init(const HWND& hWindow);
	void uninit();

	void updatePreview();
	void previewPause();
	void previewResume();
	void setDebugPointShow(bool bShow);
	void setResourcePath(const CStringW& resPath);

private:


	std::string m_resPath;
	std::chrono::steady_clock::time_point m_lastPoint;

	std::mutex m_resource_mtx; // 互斥锁.
	std::condition_variable m_resource_cv; // 条件变量.
	bool m_exit;

	DMSmartPtrT<IDMCanvas> m_pMemCanvas;
};