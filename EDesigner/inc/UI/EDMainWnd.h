//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDMainWnd.h 
// File Des: 主窗口
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-1-10	1.0		
//-------------------------------------------------------
#pragma once

class EDMainWnd : public DMHWnd, public IDMTimeline, public IDMAccelHandler, public DMTrayIconImpl<EDMainWnd>, public IEDJSonParserOwner
{
public:
	EDMainWnd();
	DECLARE_MESSAGE_MAP()						// 仿MFC消息映射宏，也可以使用BEGIN_MSG_MAPT宏使消息处理在头文件
	DECLARE_EVENT_MAP()							// 事件分发映射宏,也可以使用BEGIN_EVENT_MAPT宏使事件处理在头文件

	//---------------------------------------------------
	// Function Des: 消息分发系列函数
	//---------------------------------------------------

	int OnCreate(LPVOID);
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);
	void SetMainWndTitleText(CStringW strTitle);
	void SetMainWndTitle(bool bDataDirty);
	void OnDestroy();
	void OnSize(UINT nType, DM::CSize size);
	void OnLButtonDbClick(UINT nFlags, CPoint pt);

	bool OnAccelPressed(const DUIAccel& Accel);

	DMCode CreateSdkPreviewWnd();
	DMCode OnTimeline();
	void   ProcessCrash();
	//---------------------------------------------------
	// Function Des: 事件分发系列函数
	//---------------------------------------------------
	DMCode OnClose();
	DMCode OnBtnClose();
	DMCode OnMinimize();
	DMCode OnMaximize();
	DMCode OnRestore();
	//导入文件按钮
	DMCode OnImportReslib();
	//添加特效按钮
	DMCode OnAddTempletEffect();
	//打开预览按钮
	DMCode OnOpenPreviewPanel(DMEventArgs* pEvent);
	//关闭预览按钮
	DMCode OnClosePreviewPanel(DMEventArgs* pEvent);
	//停止Timeline
	DMCode StopPreviewTimeLine();
	//缩小预览按钮
	DMCode OnShrinkPreview(DMEventArgs* pEvent);
	//放大预览按钮
	DMCode OnEnlargePreview(DMEventArgs* pEvent);
	//压缩为zip包
	DMCode OnExportProjectToZip(DMEventArgs* pEvent);
	//暂停/继续实时预览
	DMCode OnPausePlayPreview();
	//获取保存路径
	bool SaveZipDocumentProjDlg(const CStringW& projName, CStringT& SavePath);

	void OnDropFiles(HDROP hDropInfo);
	void OnCommand(UINT uNotifyCode, int nID, HWND wndCtl);
	DMCode PopRBtnDownEffectTreeMenu(HDMTREEITEM hItem);
	DMCode PopEditorElementMenu(HDMTREEITEM hItem);
	DMCode PopEffectTreeAddBtnMenu(HDMTREEITEM hItem);

	DMCode SetParameInfoTabCurSel(int iIndex);
	int	   GetParameInfoTabCurSel();
	DMCode OnZiyuankuListboxSeleChanged();
	DMCode OnEffectTreeSeleChanged(DMEventArgs* pEvent);
	void On2DEffectItemAttrUpdate(EDPartsNodeParser* pPartsNodeParser);
	void On2DeffectItemTiggerUpdate();

	DMCode SucaiListInsertToComboItem(CStringW strTag, EDJSonParser* pJsonParser, DUIComboBox* pCombobox); //资源列表插入到2D贴纸或者美妆的资源列表中
	DMCode OnUpdateMakeupTaglistItemCombo(EDMakeupsNodeParser* pMakeupNodeParser);
	DMCode OnReloadSucaikuListbox(EDResourceNodeParser* pResourceNodeParser = NULL);
	DMCode OnRemoveSucaikuListboxItem(EDResourceNodeParser* pResourceNodeParser);
	DMCode AdjustTreeItemPos(HDMTREEITEM hItem);
	DMCode HandleEffectTreeMenu(int nID, HDMTREEITEM hItem, EDJSonParserPtr pJsonParser = NULL);
	DMCode HandleEditorElementMenu(int nID, HDMTREEITEM hItem);
protected:
	DMCode OnGlobalMenuBtn(int idFrom);
	DMCode InitFileMenu(DMXmlNode& XmlNode, int idFrom);
	DMCode InitEditMenu(DMXmlNode& XmlNode, int idFrom);
	DMCode InitLangMenu(DMXmlNode& XmlNode, int idFrom);
	DMCode InitHelpMenu(DMXmlNode& XmlNode, int idFrom);

	DMCode HandleGlobalMenu(int nID);
	DMCode HandleImpResLibMenu(int nID);
	DMCode HandlAddEffectMenu(int nID, EDJSonParserPtr pJsonParser = NULL);

	DMCode NewJsonParserCreatedNotify(EDJSonParser* pJSonParser) override;
	bool JSParseFinishTraversingEffectTree(HDMTREEITEM hItem);
	DMCode ParseJSNodeObjFinished(EDJSonParser* pJSonParser) override;
	CStringW JSonParserGetJSonFilePath() override;
	DMCode JsonParserOnFreeNotify(EDJSonParser* pJSonParser);
	DMCode RelativeResourceNodeParserChgNotify(EDJSonParser* pJSonParser) override;
	DMCode ResourceNodeOwnerParserChgNotify(EDJSonParser* pJSonParser) override;
	DMCode JSonParserDataDirtyNodtify(EDJSonParser* pJSonParser) override;

	//文档相关操作
	void HandleProjectNew(CStringW strProjectName);
	void HandleProjectOpen(CStringW strProjectPath = L"");
	DMCode HandleProjectClose();
	bool HandleProjectSave();
	bool HandleFileSaveAs(bool bSelectFolder = true, bool bPopTransitionsReminder = true);
	DMCode ReOrderParserZPositonByTreeOrder(int& iZPostion, HDMTREEITEM hItem = DMTVI_ROOT); //根据树的排序  设置parser的zposition值

	//卸载editor等控件
	DMCode UnInstallProject();
public:
	DMCode OnUpdateSettingPanelInfo(EDJSonParser* pJSonParser, bool bChgTab);
	DMCode UpdateWndPngRes(DUIWindow* pWnd, const CStringW& strPngPath);
	DMCode OnUpdateEditorElemPngPos(DUIWindow* pWnd, double fWidthScale, double fHeightScale);////更新中间编辑控件每个控件图片位置调整
	DMCode OnUpdateEditorElemPng(const CStringW& strPngPath); //更新中间编辑控件的每个控件图片
	DMCode OnUpdateEditorElemPng(ObjTreeDataPtr pData); //更新中间编辑控件的每个控件图片

	DMCode OnUpdateResPreviewWndPngPos(DUIWindow* pWnd);//图片预览的图片位置调整
	DMCode OnUpdate2dStickerPreviewPng(EDJSonParserPtr pJSonParser); //更新2d贴纸右边属性的预览图片	
	DMCode OnUpdateMakeupPreviewPng(EDJSonParserPtr pJSonParser); //更新makeup右边属性的预览图片
	DMCode OnUpdateReslibPreviewPng(const CStringW& strPngPath, DWORD dwframeCnt, int iPreviewIndex, bool bInUse, CStringW strTagInfo);
	DMCode OnZiyuankuPrevResPreviewBtnCmdEvent(DMEventArgs* pEvent);//素材库 点击上一张
	DMCode OnZiyuankuNextResPreviewBtnCmdEvent(DMEventArgs* pEvent);//素材库 点击下一张
	DMCode OnSucaikuResTagComboChangingEvent(DMEventArgs* pEvent);//素材库 素材类型combobox 真正改变
	DMCode OnSucaikuResTagComboChangeEvent(DMEventArgs* pEvent);//素材库 素材类型combobox
	DMCode OnClikSucaikuDetailBtnEvent(DMEventArgs* pEvent);//素材库 详情按钮点击
protected:
	//帧序列combobox
	DMCode On2DStickerRestComboLisChangeEvent(DMEventArgs* pEvent);
	//导入2D图片
	DMCode OnBtnImport2DImgCmdEvent(DMEventArgs* pEvent);
	//导入美妆图片
	DMCode OnBtnImportMakeupImgCmdEvent(DMEventArgs* pEvent);
	//导入图片 实现
	DMCode OnImportImgCmdImp(DUIComboBox* pListCombobox, EDJSonParserPtr pJsonParser);

	//跟踪区域combobox
	DMCode OnTracePosListComboChangeEvent(DMEventArgs* pEvent);
	//混合模式
	DMCode OnStickerBlendModeComboChangeEvent(DMEventArgs* pEvent);
	//TargetFPS Combobox select change
	DMCode OnStickerTargetFPSComboChangeEvent(DMEventArgs* pEvent);
	//贴纸 TargetFPS comboboxedit enter 输入
	DMCode OnStickerTargetFPSEditInputEnter(DMEventArgs* pEvent);
	//贴纸 TargetFPS comboboxedit 改变
	DMCode OnStickerTargetFPSEditChange(DMEventArgs* pEvent);
	//GroupSize
	DMCode OnStickerGroupSizeComboChangeEvent(DMEventArgs* pEvent);
	//GroupIndex
	DMCode OnStickerGroupIndexComboChangeEvent(DMEventArgs* pEvent);
	//AllowDiscardFrame
	DMCode OnStickerAllowDiscardFrameComboChangeEvent(DMEventArgs* pEvent);
	//StickerPostionTye
	DMCode OnStickerPositionTypeComboChangeEvent(DMEventArgs* pEvent);
	//StickerPostionRelationTye
	DMCode OnStickerPositionRelationTypeComboChangeEvent(DMEventArgs* pEvent);
	//HotLinkC
	DMCode OnStickerHotLinkComboChangeEvent(DMEventArgs* pEvent);

	//触发类型combobox
	DMCode OnTriggerAriseListComboChangeEvent(DMEventArgs* pEvent);
	//触发动作combobox
	DMCode OnTriggerActListComboChangeEvent(DMEventArgs* pEvent);
	//循环次数combobox
	DMCode OnTriggerCicleListComboChangeEvent(DMEventArgs* pEvent);

	//添加新的人脸跟踪 
	DMCode OnAddFaceTraceComboListBtn(DMEventArgs* pEvent);
	//人脸跟踪 跟踪选项
	DMCode OnFaceTraceListCombo0ChangeEvent(DMEventArgs* pEvent);
	//人脸跟踪 跟踪一
	DMCode OnFaceTraceListCombo1ChangeEvent(DMEventArgs* pEvent);
	//人脸跟踪 跟踪二
	DMCode OnFaceTraceListCombo2ChangeEvent(DMEventArgs* pEvent);
	//人脸跟踪 跟踪三
	DMCode OnFaceTraceListCombo3ChangeEvent(DMEventArgs* pEvent);
	//人脸跟踪 跟踪四
	DMCode OnFaceTraceListCombo4ChangeEvent(DMEventArgs* pEvent);
	//人脸跟踪 跟踪五
	DMCode OnFaceTraceListCombo5ChangeEvent(DMEventArgs* pEvent);

	//参数面板信息 收起展开
	DMCode OnParamInfoSetNodeAttrListExpandEvent(DMEventArgs* pEvent);
	//美妆 素材选择
	DMCode OnMakeupResListComboChangeEvent(DMEventArgs* pEvent);
	//美妆 Tag列表
	DMCode OnMakeupTagListComboChangeEvent(DMEventArgs* pEvent);
	//美妆 TargetFPS Combobox select change
	DMCode OnMakeupTargetFPSComboChangeEvent(DMEventArgs* pEvent);
	//美妆 FPS Edit输入
	DMCode OnMakeupTargetFPSEditInputEnter(DMEventArgs* pEvent);
	//美妆 FPS改变
	DMCode OnMakupTargetFPSEditChange(DMEventArgs* pEvent);
	//美妆 AllowDiscardFrame改变
	DMCode OnMakeupAllowDiscardFrameComboChangeEvent(DMEventArgs* pEvent);

	//Background Edge Width选择
	DMCode OnBackgroundEdgeWidthListComboChangeEvent(DMEventArgs* pEvent);
	//Background Edge ipaddress 控件 edit改变
	DMCode OnBackgroundEdgeColorSetCtrlEditChange(DMEventArgs* pEvt);
	//Background Edge 点击color button
	DMCode OnBackgroundEdgeColorBtnCmdEvent(DMEventArgs* pEvent);
	//FaceExchange MaxFaceSupported选择
	DMCode OnMaxFaceSupportedListComboChangeEvent(DMEventArgs* pEvent);

	//Beautify属性  EnlargeEye
	DMCode OnBeautify_EditInputEnter(DMEventArgs* pEvent);
	DMCode OnBeautify_EnlargeEye_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_EnlargeEye_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  ShrinkFace
	DMCode OnBeautify_ShrinkFace_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_ShrinkFace_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  SmallFace
	DMCode OnBeautify_SmallFace_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_SmallFace_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  Redden
	DMCode OnBeautify_Redden_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_Redden_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  Whitten
	DMCode OnBeautify_Whitten_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_Whitten_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  Smooth
	DMCode OnBeautify_Smooth_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_Smooth_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  Contrast
	DMCode OnBeautify_Contrast_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_Contrast_EditChanged(DMEventArgs* pEvent);
	//Beautify属性  Saturation
	DMCode OnBeautify_Saturation_ComboSelectChanged(DMEventArgs* pEvent);
	DMCode OnBeautify_Saturation_EditChanged(DMEventArgs* pEvent);

	void SetZiyuankuPngIndexEdtText(int);
	DMCode OnZiyuanku_PngIndex_EditChanged(DMEventArgs* pEvent);

	// 添加资源。
	DMCode ImportImages(bool bSelectFolder, bool bDisableChgTag = false, EDJSonParserPtr pJsonParserToCmp = NULL);

	DMCode ImportImageRootFile();
	bool ImportImageFile(const std::wstring& strPath, const std::wstring& strItemName, bool isDir, CStringW strResTag);

	DMCode ImportLocalMakeupImages(int nID, CStringW strMakeUpTag);
	EDResourceNodeParser* ImportMakeupArrayImages(const CArray<CStringW>& ResFilesArray, CStringW strMakeUpTag);

	DMCode BindRootWndToObjEditor(HDMTREEITEM hItem, EffectTempletType id, EDJSonParserPtr pJSonParser = NULL);
	DMCode BindObjTreeData(DUIRoot* pEditor, DUIWindowPtr pDUI, HDMTREEITEM hTreeItem, EffectTempletType iEfType, EDJSonParserPtr pJSonParser = NULL);
public:
	// 填充Transition树控件
	HDMTREEITEM AddEffectNodeItem(HDMTREEITEM parent, LPCWSTR lpName, bool bEyeCheck, bool bDisableEyeCheck, bool bSelectItem, EffectTempletType type /*= EFFECTTEMPBTN_2DSTIKERNODE*/, EDJSonParserPtr pJSonParser = NULL);
	VOID AdjustTreeItemPosByZPositon(HDMTREEITEM hItem);//冒泡法排序
public:
	DMSmartPtrT<SDKPreviewWnd>          m_pSDKPreviewWnd;
	DUIObjEditor*						m_pObjEditor;
	HDMTREEITEM							m_hObjSel;
	ActionSlotMgr<ActionSlot>			m_actionSlotMgr;
	
public:
	EDJSonMainParser					m_JSonMainParser;
	CStringW							m_strProjectName;
	CStringW							m_strJSonFilePath;
	CStringW							m_strJSonFileJSonPath;

	ED::DUIEffectTreeCtrl*				m_pEffectTreeCtrl;
	DUIComboBox*						m_pMakeupResListCombobox; //美妆混合素材类型 列表
	DUIComboBox*						m_p2DStickerResListCombobox; //序列帧 列表
private:
	DUIAccel							m_CtrlZAccel;
	DUIAccel							m_CtrlRAccel;
	DUIAccel							m_CtrlSAccel;
	DUIAccel							m_CtrlNAccel;
	DUIAccel							m_F12Accel; //另存为
	DUIAccel							m_CtrlQAccel;
	DUIAccel							m_CtrlOAccel;
	DUIButton*                          m_pMaxBtn;
	DUIButton*                          m_pRestoreBtn;
	ED::DUITransitionTreeCtrl*			m_pTransitionTreeCtrl;
	DUIStatic*							m_pStaTransitionName;
	ED::EDListBoxEx*					m_pZiyuankuListboxEx;
	DUIComboBox*						m_pTraceposListCombobox; //跟踪区域 列表
	DUIComboBox*						m_pStickerBlendModeCombobox; //混合 列表
	DUIComboBox*						m_pStickerTargetFPSCombobox; //TargetFps 列表
	DUIComboBox*						m_pStickerGroupSizeCombobox; //GroupSize 列表
	DUIComboBox*						m_pStickerGroupIndexCombobox; //GroupIndex 列表
	DUIComboBox*						m_pStickerAllowDiscardFrameCombobox; //AllowDiscardFrame 列表
	DUIComboBox*						m_pStickerPositionTypeCombobox;//PostionType列表
	DUIComboBox*						m_pStickerPositionRelationTypeCombobox;//PostionRelationType列表
	DUIComboBox*						m_pStickerHotLinkCombobox;//HotLink列表

	DUIList*							m_p2DStickerNodeAttrList; //2d贴纸属性界面
	DUIList*							m_pTransitionNodeAttrList; //Transition节点界面
	DUIList*							m_pMakeupNodeAttrList; //美妆子节点界面
	DUIList*							m_pMakeupMajorAttrList; //美妆父节点界面
	DUIList*							m_pBeautifyAttrList; //Beautify属性节点界面
	DUIList*							m_pFaceExchangeAttrList; //FaceExchange属性节点界面
	DUIList*							m_pBackgroundEdgeAttrList; //background edge属性节点界面

	DUIComboBox*						m_pMakeupTagListCombobox; //美妆标签类型 列表
	DUIComboBox*						m_pMakeupTargetFPSCombobox; //美妆TargetFps列表
	DUIComboBox*						m_pMakeupAllowDiscardFrameCombobox; //美妆AllowDiscardFrame列表
	DUIComboBox*						m_pTriggerAriseListCombobox; //触发类型 列表
	DUIComboBox*						m_pTriggerActListCombobox; //触发动作 列表
	DUIComboBox*						m_pTriggercicleListCombobox; //循环次数 列表
	HDMTREEITEM							m_hTreeAddPopupItem;		//弹出菜单的那个tree的item

	DUIComboBox*						m_pFaceTraceListCombobox0; //人脸跟踪 跟踪选项
	DUIComboBox*						m_pFaceTraceListCombobox1; //人脸跟踪 跟踪一
	DUIComboBox*						m_pFaceTraceListCombobox2; //人脸跟踪 跟踪二
	DUIComboBox*						m_pFaceTraceListCombobox3; //人脸跟踪 跟踪三
	DUIComboBox*						m_pFaceTraceListCombobox4; //人脸跟踪 跟踪四
	DUIComboBox*						m_pFaceTraceListCombobox5; //人脸跟踪 跟踪五
	DUIButton*							m_pAddFaceTraceComboListBtn; //添加人脸跟踪按钮

	DUIComboBox*						m_pMaxFaceSuppportedListCombobox;	//FaceExchange -> MaxFaceSuppported
	DUIComboBox*						m_pBackgroundEdgeWidthListCombobox;	//background edge -> width
	DUIPAddressCtrl*					m_pBackgroundEdgeColorSetCtrl;		//background edge -> IpAddress
	DUIButton*							m_pBackgroundEdgeColorBtn;			//background edge -> width
	DUIComboBox*						m_pBeautifyEnlargeEyeCombobox;		//Beautify -> enlargeEye
	DUIComboBox*						m_pBeautifyShrinkFaceCombobox;		//Beautify -> ShrinkFace
	DUIComboBox*						m_pBeautifySmallFaceCombobox;		//Beautify -> smallFace
	DUIComboBox*						m_pBeautifyReddenCombobox;			//Beautify -> redden
	DUIComboBox*						m_pBeautifyWhittenCombobox;			//Beautify -> whitten
	DUIComboBox*						m_pBeautifySmoothCombobox;			//Beautify -> smooth
	DUIComboBox*						m_pBeautifyConstrastCombobox;		//Beautify -> contrast
	DUIComboBox*						m_pBeautifySaturtionCombobox;		//Beautify -> saturation
	DUIRichEdit*						m_pZiyuankuPngIndexEdt;				//资源信息面板  PngIndex Edit

	DMSmartPtrT<DM::PosEdit>            m_pMakeUpPosEditItem[7];//美妆面板 位置信息的edit
	DMSmartPtrT<DM::PosEdit>            m_pStickerPosEditItem[7];//2d贴纸面板 位置信息的edit

	int									m_iStickerTargetFPS;
	int									m_iMakeupTargetFPS;
	bool								m_bShrinkingPreview;
	int									m_iShrinkScale;
	int									m_iShrinkScaleDest;
	bool								m_bProjectAddedMakeupRes[20];
	bool								m_bJsonDataDirtyMark;
	int									m_iTransitionIndex;

private:
	double m_dbEnlargeRatio;
	double m_dbShrinkRatio;
	double m_dbSmallRatio;
	double m_dbReddenStrength;
	double m_dbWhitenStrength;
	double m_dbSmoothStrength;
	double m_dbContrastStrength;
	double m_dbSaturation;
	DMColor	  m_iBackGroundEdgeColor;
};
