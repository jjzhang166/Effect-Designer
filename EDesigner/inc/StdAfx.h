// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	StdAfx.h
// File mark:   
// File summary:代理头文件
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-1-10
// ----------------------------------------------------------------
#pragma once

#include "EDBaseOutput.h"

// lib
#ifdef _DEBUG
#pragma comment(lib,"EDBase_d.lib")
#pragma comment(lib, "zlib_d.lib")
#else
#pragma comment(lib,"EDBase.lib")
#pragma comment(lib, "zlib.lib")
#endif

#pragma comment(lib, "comctl32.lib")

using namespace DM;
/// 头文件依赖

// 0 
#include "EDDef.h"
#include "EDUIPlugin.h"
#include "EDBase.h"
#include "EDJSonParser.h"
#include "EDResourceParser.h"
#include "EDJSonMainParser.h"
#include "EDFaceMorphParser.h"
#include "EDPartsParser.h"
#include "EDTransitionParser.h"
#include "EDBeautifyParser.h"
#include "EDFaceExchangeParser.h"
#include "EDBackgroundEdge.h"

// 1 DM
#include "IDMPlugin.h"
#include "DUIIE.h"
#include "DUIMenu.h"
#include "DUIButton.h"
#include "DUITabCtrl.h"
#include "DUIEdit.h"
#include "DUIAccel.h"
#include "DUIListBox.h"
#include "DUIListBoxEx.h"
#include "DUIStatic.h"
#include "DUICheckBox.h"
#include "IDMPlugin.h"
#include "DUITreeCtrlEx.h"
#include "DUIComboBox.h"
#include "DUIPAddressCtrl.h"
#include "DUIFlowLayout.h"

// 3.DUI
#include "DUIEffectTreeCtrl.h"
#include "EDListBoxEx.h"
#include "DUIList.h"
#include "EDMsgBox.h"
#include "EDResEditDlg.h"
#include "EDResSelectDlg.h"
#include "NewResDlg.h"

#include "Layout.h"
#include "DUIRoot.h"
#include "Data.h"
#include "DUITransitionTreeCtrl.h"
#include "DUIDragFrame.h"
#include "DUIPos.h"
#include "DUIAlignmentFrame.h"
#include "DUIObjEditor.h"

#include "ActionSlot.h"
#include "ActionSlotMgr.h"
#include "DUIWndAutoMuteGuard.h"

#include "DUIReferenceLine.h"
#include "DUIPreviewPanel.h"
#include "SDKPreviewWnd.h"
#include "DMTrayIconImpl.h"

#include "EDMainWnd.h"
#include "DMTooltipReImpl.h"
#include "DUISplitLayoutEx.h"

//other
#include "Helper.h"
#include "Events.h"

// 4.Core
#include "EDMain.h"

// 5.zip
 #include "DMResZipImpl.h"
 #include "DMResZipParam.h"

//res
#include "resource.h"


#ifdef USE_DMSKIA_RENDER_
#pragma warning (disable: 4005)  
#include "SkCanvas.h"
#include "SkBitmap.h"
#include "SkTypeface.h"
#include "SkShader.h"
#include "sktdarray.h"
#include "SkGraphics.h"
#include "skcolorpriv.h"
#include "SkXfermode.h"
#include "effects\SkDashPathEffect.h"
#include "effects\SkGradientShader.h"
#include "ports\SkTypeface_win.h"
#include "src\core\SkReadBuffer.h"

#include "DMSkiaRenderImpl.h"
#ifdef _DEBUG
#pragma comment(lib, "Skia_d.lib")
#else
#pragma comment(lib, "Skia.lib")
#endif
#endif//USE_DMSKIA_RENDER_

using namespace ED;