//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDBackgroundEdge.h 
// File Des: backgroundEdge字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-6-8	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDBackgroundEdgeParserAtrr
	{
	public:
		static char* INT_color;								///< 示例:"color": -1
		static char* BOOL_enable;							///< 示例:"enable" : true
		static char* INT_width;								///< 示例:"width": 5
		static char* INT_zposition;							///< 示例:"zposition" : 4
	};
	EDAttrValueInit(EDBackgroundEdgeParserAtrr, INT_color)EDAttrValueInit(EDBackgroundEdgeParserAtrr, BOOL_enable)
	EDAttrValueInit(EDBackgroundEdgeParserAtrr, INT_width)EDAttrValueInit(EDBackgroundEdgeParserAtrr, INT_zposition)
}

/// <summary>
///		"BackgroundEdge"节点json字段解析类
/// </summary>
class EDBackgroundEdgeParser : public EDJSonParser
{
	EDDECLARE_CLASS_NAME(EDBackgroundEdgeParser, L"backgroundEdge", DMREG_Attribute);
public:
	EDBackgroundEdgeParser();
	~EDBackgroundEdgeParser();

	DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
	DMCode BuildJSonData(JSHandle JSonHandler) override;
	DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

	int GetZPositionOrder() override;
	bool SetZPositionOrder(int iZPostion) override;
	bool IsParserEnable() override;															///< 获取m_bEnable

public:
	int m_iColor;
	bool m_bEnable;
	int m_iWidth;
	int m_iZPosition;
};