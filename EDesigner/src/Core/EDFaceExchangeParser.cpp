#include "StdAfx.h"
#include "EDFaceExchangeParser.h"

EDFaceExchangeParser::EDFaceExchangeParser()
	: m_iMaxFaceSupported(2)
	, m_iZPosition(-1)
{
}

EDFaceExchangeParser::~EDFaceExchangeParser()
{
}

DM::DMCode EDFaceExchangeParser::SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon)
{
	DMCode iErr = DM_ECODE_FAIL;
	if (0 == _stricmp(pszAttribute, EDAttr::EDEDFaceExchangeParserAtrr::INT_maxFaceSupported))
	{
		if (JsHandleValue.isInt())
		{
			m_iMaxFaceSupported = JsHandleValue.toInt();
			iErr = DM_ECODE_OK;
		}
	}
	else if (0 == _stricmp(pszAttribute, EDAttr::EDEDFaceExchangeParserAtrr::INT_zPosition))
	{
		if (JsHandleValue.isInt())
		{
			m_iZPosition = JsHandleValue.toInt();
			iErr = DM_ECODE_OK;
		}
	}
	else
	{//强制设置为OK 避免后面可能new出其他节点出来   因为已经不可能有子节点了
		DMASSERT_EXPR(false, L"FaceExchange 有没有解析的节点");
		iErr = DM_ECODE_NOLOOP;
	}

	if (!bLoadJSon)
		MarkDataDirty();
	return iErr;
}

DM::DMCode EDFaceExchangeParser::BuildJSonData(JSHandle JSonHandler)
{
	if (m_strJSonMemberKey.IsEmpty())
	{
		m_strJSonMemberKey = std::string(EDBASE::w2a(GetClassNameW())).c_str();
	}
	JSHandle JSonChild = JSonHandler[(LPCSTR)m_strJSonMemberKey];
	return EDJSonParser::BuildJSonData(JSonChild);
}

DM::DMCode EDFaceExchangeParser::BuildMemberJsonData(JSHandle &JSonHandler)
{
	JSonHandler[EDAttr::EDEDFaceExchangeParserAtrr::INT_maxFaceSupported].putInt(m_iMaxFaceSupported);
	JSonHandler[EDAttr::EDEDFaceExchangeParserAtrr::INT_zPosition].putInt(m_iZPosition);
	return DM_ECODE_FAIL;
}

int EDFaceExchangeParser::GetZPositionOrder()
{
	return m_iZPosition;
}

bool EDFaceExchangeParser::SetZPositionOrder(int iZPostion)
{
	m_iZPosition = iZPostion;
	return true;
}