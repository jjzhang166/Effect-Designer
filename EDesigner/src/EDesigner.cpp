﻿// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDesigner.cpp
// File mark:   主入口
// File summary:
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-1-10
// ----------------------------------------------------------------
#pragma once
#include "StdAfx.h"

#if (_MSC_VER==1500)
#ifdef _DEBUG
#if 0
#include "vld.h"
#pragma comment(lib, "vld.lib")// VLD仅在mDd模式下才能有效，其他v s版本请使用其他版本vld
#endif
#endif 
#endif

//#define  _USEMEMLEAK

#ifdef _USEMEMLEAK
#include "MemLeak.h"
class  MemLeakCollect
{
public:
	MemLeakCollect(){
		CMemLeak::Instance().Init();
	}
	~MemLeakCollect() {
		CMemLeak::Instance().Uninit();
	}
private:
}memleak;
#endif // !_USEMEMLEAK

int APIENTRY _tWinMain(HINSTANCE hInstance,
					   HINSTANCE hPrevInstance,
					   LPTSTR    lpCmdLine,
					   int       nCmdShow)
{
	OleInitialize(NULL);
	EDUIPlugin plugin;
	DMApp theApp(hInstance); 
	theApp.InstallPlugin(&plugin);
	EDMain theMain(hInstance);
	return (int) 0;
}