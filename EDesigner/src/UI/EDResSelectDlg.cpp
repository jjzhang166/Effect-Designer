#include "StdAfx.h"
#include "EDResSelectDlg.h"

int Show_ResSelectDlg(LPCWSTR lpResTag, EDJSonParser* pJsonParser, HWND hWnd)
{
	DMSmartPtrT<EDResSelectDlg> pDlg; 
	pDlg.Attach(new EDResSelectDlg());
	if (NULL == hWnd)
	{
		hWnd = g_pMainWnd->m_hWnd;
	}
	pDlg->m_strTag = lpResTag;
	pDlg->m_pJsonParser = pJsonParser;
	int iRet = pDlg->DoModal(L"ds_resselectdlg", hWnd);
 	if (g_pMainWnd && g_pMainWnd->IsWindow())
 	{
 		g_pMainWnd->SetActiveWindow();
 	}
	return iRet;
}

BEGIN_MSG_MAP(EDResSelectDlg)
	MSG_WM_INITDIALOG(OnInitDialog) 
	MSG_WM_SIZE(OnSize)
	CHAIN_MSG_MAP(DMHDialog)
END_MSG_MAP()
BEGIN_EVENT_MAP(EDResSelectDlg)
	EVENT_NAME_COMMAND(L"closebutton", OnBtnClose)
	EVENT_NAME_COMMAND(L"ds_cancelbtn", OnBtnClose)
	EVENT_NAME_COMMAND(L"ds_okbtn", OnBtnOk)
	EVENT_NAME_COMMAND(L"ds_importimgbtn", OnBtnImportImg)
END_EVENT_MAP()

EDResSelectDlg::EDResSelectDlg() : m_pJsonParser(NULL)
{
}  

BOOL EDResSelectDlg::OnInitDialog(HWND wndFocus, LPARAM lInitParam)
{
	DUIComboBox* pCombobox = FindChildByNameT<DUIComboBox>(L"ds_ResSelectCombobox"); DMASSERT(pCombobox);
	g_pMainWnd->SucaiListInsertToComboItem(m_strTag, m_pJsonParser, pCombobox);
	if (pCombobox)
	{
		if (pCombobox->GetCount() == 0)
		{
			pCombobox->InsertItem(0, L"��");
			pCombobox->SetCurSel(0);
			DUIButton* pOkBtn = FindChildByNameT<DUIButton>(L"ds_okbtn");
			if (pOkBtn)
			{
				pOkBtn->SetAttribute(L"bdisable", L"1");
			}
		}
		else
			pCombobox->SetCurSel(pCombobox->GetCount()-1);
	}

	return TRUE;
}

void EDResSelectDlg::OnSize(UINT nType, CSize size)
{
	if (!IsIconic()) 
	{
		CRect rcWnd;
		::GetWindowRect(m_hWnd, &rcWnd);
		::OffsetRect(&rcWnd, -rcWnd.left, -rcWnd.top);  
		HRGN hWndRgn = ::CreateRoundRectRgn(0, 0, rcWnd.right, rcWnd.bottom,4,4);
		SetWindowRgn(hWndRgn, TRUE);
		::DeleteObject(hWndRgn); 
	}             
	SetMsgHandled(FALSE);
}

DMCode EDResSelectDlg::OnBtnClick(int uID)
{
	EndDialog(uID);
	if (g_pMainWnd && g_pMainWnd->IsWindow())
	{
		g_pMainWnd->SetActiveWindow();
	}
	return DM_ECODE_OK;
}

DMCode EDResSelectDlg::OnBtnClose()
{
	return OnBtnClick(IDCANCEL);
}

DMCode EDResSelectDlg::OnBtnOk()
{
	DUIComboBox* pCombobox = FindChildByNameT<DUIComboBox>(L"ds_ResSelectCombobox"); DMASSERT(pCombobox);
	if (pCombobox && m_pJsonParser)
	{
		LPARAM lpData = pCombobox->GetItemData(pCombobox->GetCurSel()); DMASSERT(lpData);
		if (lpData)
		{
			m_pJsonParser->RelativeResourceNodeParser((EDResourceNodeParser*)lpData);
		}
	}
	else
		DMASSERT(FALSE);
	return OnBtnClick(IDOK);
}

DMCode EDResSelectDlg::OnBtnImportImg()
{
	return OnBtnClick(IDCONTINUE);
}