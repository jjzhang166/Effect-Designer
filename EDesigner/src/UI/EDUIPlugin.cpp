#include "StdAfx.h"
#include "EDUIPlugin.h"

const wchar_t* EDUIPlugin::GetName() const
{
	return L"{5S953S41-5324-4E2B-EE09-3E68C4296DSF}";
}

void EDUIPlugin::Install()
{
	g_pDMApp->Register(DMRegHelperT<DUIList>(), true);
	g_pDMApp->Register(DMRegHelperT<DMResZipImpl>(), true);
	g_pDMApp->Register(DMRegHelperT<ED::DUIEffectTreeCtrl>(), true);
	g_pDMApp->Register(DMRegHelperT<ED::EDListBoxEx>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIObjEditor>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIRoot>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIDragFrame>(), true);
	g_pDMApp->Register(DMRegHelperT<PosEdit>(), true);
	g_pDMApp->Register(DMRegHelperT<PosItem>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIPos>(), true);
	g_pDMApp->Register(DMRegHelperT<Layout>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIReferenceLine>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIAlignmentFrame>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIPreviewPanel>(), true);
	g_pDMApp->Register(DMRegHelperT<DUITransitionTreeCtrl>(), true);
	g_pDMApp->Register(DMRegHelperT<DMTooltipReImpl>(), true);
	g_pDMApp->SetDefRegObj(Layout::GetClassName(), Layout::GetClassType());	

	//json解析类
	g_pDMApp->Register(DMRegHelperT<EDJSonParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDResourceParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDResourceNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDFaceMorphParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDMakeupsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDMakeupsNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDPartsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDPartsNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDTransitionsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDTransitionsNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDConditionsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDConditionsNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDTargetsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDTargetsNodeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDBeautifyPartsParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDBeautifyParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDFaceExchangeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<EDBackgroundEdgeParser>(), true);
	g_pDMApp->Register(DMRegHelperT<DM::DUISplitLayoutEx>(), true);
	g_pDMApp->Register(DMRegHelperT<DUIRecentListBox>(), true);
#ifdef USE_DMSKIA_RENDER_
	DMCode iErr = g_pDMApp->Register(DMRegHelperT<DMSkiaRenderImpl>(), true);
 	DMASSERT_EXPR(DMSUCCEEDED(iErr), L"注册DMSkiaRenderImpl失败！");
 	iErr = g_pDMApp->SetDefRegObj(DMSkiaRenderImpl::GetClassName(), DMSkiaRenderImpl::GetClassType());
 	DMASSERT_EXPR(DMSUCCEEDED(iErr), L"设置DMSkiaRenderImpl为默认Render对象失败！");
#endif
}

void EDUIPlugin::Uninstall()
{
	g_pDMApp->UnRegister(DUIList::GetClassName(), DUIList::GetClassType());
	g_pDMApp->UnRegister(DMResZipImpl::GetClassName(), DMResZipImpl::GetClassType());
	g_pDMApp->UnRegister(ED::DUIEffectTreeCtrl::GetClassName(), ED::DUIEffectTreeCtrl::GetClassType());
	g_pDMApp->UnRegister(ED::EDListBoxEx::GetClassName(), ED::EDListBoxEx::GetClassType());
	g_pDMApp->UnRegister(DUIObjEditor::GetClassName(), DUIObjEditor::GetClassType());
	g_pDMApp->UnRegister(DUIRoot::GetClassNameW(), DUIRoot::GetClassType());
	g_pDMApp->UnRegister(DUIDragFrame::GetClassNameW(), DUIDragFrame::GetClassType());
	g_pDMApp->UnRegister(PosEdit::GetClassNameW(), PosEdit::GetClassType());
	g_pDMApp->UnRegister(PosItem::GetClassNameW(), PosItem::GetClassType());
	g_pDMApp->UnRegister(DUIPos::GetClassNameW(), DUIPos::GetClassType());
	g_pDMApp->UnRegister(Layout::GetClassNameW(), Layout::GetClassType());
	g_pDMApp->UnRegister(DUIAlignmentFrame::GetClassNameW(), DUIAlignmentFrame::GetClassType());
	g_pDMApp->UnRegister(DUIReferenceLine::GetClassNameW(), DUIReferenceLine::GetClassType());
	g_pDMApp->UnRegister(DUIPreviewPanel::GetClassNameW(), DUIPreviewPanel::GetClassType());
	g_pDMApp->UnRegister(DUITransitionTreeCtrl::GetClassNameW(), DUITransitionTreeCtrl::GetClassType());
	g_pDMApp->UnRegister(DMTooltipReImpl::GetClassNameW(), DMTooltipReImpl::GetClassType());
	//json解析类
	g_pDMApp->UnRegister(EDJSonParser::GetClassNameW(), EDJSonParser::GetClassType());
	g_pDMApp->UnRegister(EDResourceParser::GetClassNameW(), EDResourceParser::GetClassType());
	g_pDMApp->UnRegister(EDResourceNodeParser::GetClassNameW(), EDResourceNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDFaceMorphParser::GetClassNameW(), EDFaceMorphParser::GetClassType());
	g_pDMApp->UnRegister(EDMakeupsParser::GetClassNameW(), EDMakeupsParser::GetClassType());
	g_pDMApp->UnRegister(EDMakeupsNodeParser::GetClassNameW(), EDMakeupsNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDPartsParser::GetClassNameW(), EDPartsParser::GetClassType());
	g_pDMApp->UnRegister(EDPartsNodeParser::GetClassNameW(), EDPartsNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDTransitionsParser::GetClassNameW(), EDTransitionsParser::GetClassType());
	g_pDMApp->UnRegister(EDTransitionsNodeParser::GetClassNameW(), EDTransitionsNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDConditionsParser::GetClassNameW(), EDConditionsParser::GetClassType());
	g_pDMApp->UnRegister(EDConditionsNodeParser::GetClassNameW(), EDConditionsNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDTargetsParser::GetClassNameW(), EDTargetsParser::GetClassType());
	g_pDMApp->UnRegister(EDTargetsNodeParser::GetClassNameW(), EDTargetsNodeParser::GetClassType());
	g_pDMApp->UnRegister(EDBeautifyPartsParser::GetClassNameW(), EDBeautifyPartsParser::GetClassType());
	g_pDMApp->UnRegister(EDBeautifyParser::GetClassNameW(), EDBeautifyParser::GetClassType());
	g_pDMApp->UnRegister(EDFaceExchangeParser::GetClassNameW(), EDFaceExchangeParser::GetClassType()); 
	g_pDMApp->UnRegister(EDBackgroundEdgeParser::GetClassNameW(), EDBackgroundEdgeParser::GetClassType());
	g_pDMApp->UnRegister(DUISplitLayoutEx::GetClassNameW(), DUISplitLayoutEx::GetClassType());
	g_pDMApp->UnRegister(DUIRecentListBox::GetClassNameW(), DUIRecentListBox::GetClassType());
#ifdef USE_DMSKIA_RENDER_
	g_pDMApp->UnRegister(DMSkiaRenderImpl::GetClassNameW(), DMSkiaRenderImpl::GetClassType());
#endif
}

void EDUIPlugin::Initialise()
{
}

void EDUIPlugin::Shutdown()
{
}